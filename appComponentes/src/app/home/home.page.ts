import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  titulo = "Police Series App";
  series = [
    {
      titulo: 'Lucifer',
      subtitulo: '',
      capa: 'https://image.ibb.co/hiiOHd/thumb_1920_694600.jpg',
      texto: "Lucifer Morningstar, o famigerado Senhor do Inferno, se cansa da vida demoníaca que leva no subterrâneo e abandona seu trono para tirar férias em Los Angeles. Lá, ele abre uma casa noturna chamada Lux e passa a desfrutar de tudo o que mais gosta: vinho, mulheres e excessos. Sua nova rotina se complica quando uma estrela do pop é brutalmente assassinada diante de seus olhos. Pela primeira vez em mais de 10 milhões de anos, Lucifer sente despertar em seu interior um desejo de justiça e resolve punir os responsáveis pelo crime. "
    },
    {
      titulo: "Brooklyn Nine-Nine",
      subtitulo: "",
      capa: "https://streamingsbrasil.com/wp-content/uploads/2022/01/Brooklyn-99-Temp-9-Thumbnail-1130x580.jpg",
      texto: "Brooklyn Nine-Nine (abreviado como B99) é uma série de televisão de comédia policial americana criada por Dan Goor e Michael Schur. A série gira em torno de Jake Peralta, um imaturo, mas talentoso, detetive da polícia de Nova York na fictícia 99.ª Delegacia do Brooklyn, que muitas vezes entra em conflito com seu novo comandante, o sério e severo capitão Raymond Holt."
    },
    {
      titulo: "Sherlock",
      subtitulo: "",
      capa: "https://th.bing.com/th/id/OIP.KzgpLboRojKRpAJoETiKewHaEK?pid=ImgDet&rs=1",
      texto: "O Dr. John Watson precisa de um lugar para morar em Londres. Ele é apresentado ao detetive Sherlock Holmes e os dois acabam desenvolvendo uma parceria intrigante, na qual a dupla vagará pela capital inglesa solucionando assassinatos e outros crimes brutais. Tudo isso em pleno século XXI."
    },
    {
      titulo: "Arcanjo Renegado",
      subtitulo: "",
      capa: "https://1.bp.blogspot.com/-zLGwp_M2z3Y/YBzIQ3-qJrI/AAAAAAAATKw/aFrcvdczov4sAFz3hv0GRM-D7NRwEYYcQCNcBGAsYHQ/s585/arcanjo-renegado-nova-serie-do-Globoplay.jpg",
      texto: "Arcanjo Renegado acompanha a história de Mikhael, um sargento incorruptível, comandante de uma equipe do BOPE no Rio de Janeiro. Depois de se envolver em uma operação que terminou em chacina, ele é transferido para uma unidade policial no interior do estado. Mikhael, enlouquecido de raiva, busca vingança pela morte de um amigo e colega de equipe. Nessa perigosa busca por justiça com as próprias mãos, ele enfrenta pessoas poderosas e corruptas e acaba adentrando o sombrio mundo do crime organizado do Rio de Janeiro."
    },
    {
      titulo: "24: Legacy",
      subtitulo: "",
      capa: "https://images.fanart.tv/fanart/24-legacy-58a3558248224.jpg",
      texto: "Eric Carter é um herói militar que acaba de voltar para casa, mas seu retorno aos Estados Unidos não será tão tranquilo quanto ele gostaria. Correndo risco de vida e sem ter a quem recorrer, Eric pede ajuda ao CTU, a agência federal contra-terrorismo, e quando se dá conta, já está liderando uma missão para impedir um dos maiores ataques terroristas em solo norte-americano."
    },
    {
      titulo: "Prison Break",
      subtitulo: "",
      capa: "https://a-static.mlcdn.com.br/1500x1500/caneca-prison-break-store/presentescriativos/9331047844/0884d44cacaa9a7af05a0b67ef1a0556.jpg",
      texto: "Após a prisão de Lincoln Burrows, condenado por um crime que não cometeu, o engenheiro Michael Scofield bola um plano para tirar o irmão da cadeia. Enviado para Fox River ao lado de Lincoln, Michael começa a executar a sua estratégia, mas logo percebe que está no meio de uma perigosa conspiração. Para garantir a liberdade da sua família, ele precisará enganar a Dra. Sara Tancredi e se associar a criminosos condenados, como Fernando Sucre, Theodore 'T-Bag' Bagwell e John Abruzzi."
    }, 
    {
      titulo: "Jessica Jones",
      subtitulo: "",
      capa: "https://oretalho.com.br/wp-content/uploads/2019/11/2019-11-01-jessica-jones-capa-season-2.jpg",
      texto: "Desde que sua curta vida como super-heroína acabou de forma trágica, Jessica Jones vem reconstruindo sua carreira e passou a levar a vida como detetive particular no bairro de Hell's Kitchen, em Nova York, na sua própria agência de investigações, a Alias Investigations. Traumatizada por eventos anteriores de sua vida, ela sofre de Transtorno de Estresse Pós-Traumático, e tenta fazer com que seus super-poderes passem despercebidos pelos seus clientes. Mas, mesmo tentando fugir do passado, seus demônios particulares vão voltar a perseguí-la, na figura de Zebediah Kilgrave, um obsessivo vilão que fará de tudo para chamar a atenção de Jessica. "
    },
    {
      titulo: "Quem Matou Sara?",
      subtitulo: "",
      capa: "https://observatoriodocinema.uol.com.br/wp-content/uploads/2021/05/Quem-matou-sara-divulgacao.jpg",
      texto: "Quem Matou Sara? conta a história de Alejandro Guzmán, um homem que foi injustamente acusado do assassinato de sua irmã Sara. Depois de passar 18 anos na prisão, Álex enfim está livre e parece determinado a fazer justiça com as próprias mãos. Para descobrir a verdade por trás do caso e provar sua inocência, ele decide se reaproximar da família Lazcano, responsável por incriminá-lo no passado. Mas o poderoso clã não vai medir esforços para manter seus segredos fora do jogo - mesmo que isso signifique acobertar o verdadeiro culpado pelo crime. Como a morte de Sara aconteceu há quase duas décadas, Álex vai precisar viajar no tempo para juntar todas as peças do quebra-cabeça e, assim, se vingar daqueles que destruíram sua vida. "
    }
  ];

  constructor() {}

}
